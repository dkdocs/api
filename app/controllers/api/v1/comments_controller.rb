class Api::V1::CommentsController < ApplicationController

  def index
    # binding.pry
    if params[:topic_id] && params[:after]
      topic = Topic.find_by_id(params[:topic_id])
      if topic.present?
        comments = topic.comments.limit(10).offset(params[:after]).select("id, topic_id, substring(message from 1 for 400) as message,email,score, created_at").order('created_at desc')
        if comments.present?
          render json: {comments: comments, meta: meta_response(200, "ok")}
        else
          render json: {meta: meta_response(404,"Comments not found.")}
        end
      else
        render json: {meta: meta_response(404, "Topic not found")}
      end
    else
      render json: {meta: meta_response(400, "Invalid Params")}
    end
  end


  def create
    comment = Comment.new(permitted_params)
    if comment.save
      render json: { comment: comment, meta: meta_response(200,"Comment created") }
    else
      render json: { meta: meta_response(400, comment.errors.full_messages.join(','))}
    end
  end


  def update
    if params[:vote] and params[:id] and [1,-1].include?( params[:vote])
      comment = Comment.find(params[:id])
      comment.score = comment.score.present? ? comment.score += params[:vote] : params[:vote]
      if comment.save  
        render json: {:comment_id => comment.id, :score => comment.score, meta: meta_response(201, "success")}
      else
        render json: { meta: meta_response(500, "Something went wrong") }
      end
    else
      render json: { meta: meta_response(422, "missing parameters") }
    end
  end

  def complete_comment
    if params[:id] and Comment.find(params[:id])
      message = Comment.find(params[:id]).message
      render json: {message: message, meta: meta_response(200,"OK")}
    else
      render json: { meta: meta_response(404, "Invalid Id") }
    end
  end


  private

  def permitted_params
    params.permit(:message, :topic_id, :score, :email)
  end

end
