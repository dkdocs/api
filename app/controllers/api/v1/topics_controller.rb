class Api::V1::TopicsController < ApplicationController
  swagger_controller :topics, 'Topics'

  swagger_api :index do
    summary 'Returns all topics'
    notes 'Notes...'
  end

  def index
    @topics = Topic.all.order(created_at: :asc)
    render json: @topics, status: :ok
  end

  def show
  	topic = Topic.find_by_id(params[:id])
  	if topic.present?
  		render json: {topic: topic, meta:  meta_response(200, "ok")}
  	else
  		render json: {meta: meta_response(404, "Topic not found")}
  	end
  end
end
