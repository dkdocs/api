class Comment < ActiveRecord::Base
  belongs_to :topic

  VALID_EMAIL_REGEX = /\A[\w+\-.]+@[a-z\d\-]+(\.[a-z\d\-]+)*\.[a-z]+\z/i
  
  validates :email, presence: true, format: {with: VALID_EMAIL_REGEX}
  validates :topic_id, presence: true

end
