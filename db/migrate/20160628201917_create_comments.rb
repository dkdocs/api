class CreateComments < ActiveRecord::Migration
  def change
    create_table :comments do |t|
      t.references :topic, index: true, foreign_key: true
      t.string :email
      t.string :message
      t.integer :score

      t.timestamps null: false
    end
  end
end
