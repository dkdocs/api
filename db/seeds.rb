# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

Topic.create([ 
		{ name: 'Web Design'},
		{ name: 'Photography'},
		{ name: 'Trekking'},
		{ name: 'Food'},
		{ name: 'Adventure'},
		{ name: 'Music'},
		{ name: 'Politics'},
		{ name: 'Game Of Thrones'},
		{ name: 'Weather'},
		{ name: 'Football'}

	])